/**
 * Created by tomaszr on 2016-04-24.
 */
(function() {
    'use strict';

    angular
        .module('app.landing')
        .controller('LandingController', LandingController);

    LandingController.$inject = ['$rootScope', 'bookService'];

    function LandingController($rootScope, bookService) {
        var vm = this;

        bookService.getBooks().then(function(data) {
          vm.books = data;
        });
    }

})();
