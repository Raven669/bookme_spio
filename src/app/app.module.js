(function() {
  'use strict';

  angular
    .module('app', [
      // Angular modules.
      'ngRoute',

      // Custom modules.
      'app.auth',
      'app.core',
      'app.landing',
      'app.list',
      'app.layout',
      'app.book',
      'app.userBooksList',
      'ng-token-auth'
    ])
    .config(configFunction)
    .run(runFunction);

  configFunction.$inject = ['$routeProvider', '$authProvider'];

  function configFunction($routeProvider, $authProvider) {
    $routeProvider.otherwise({
      redirectTo: '/'
    });

    $authProvider.configure({
      apiUrl: 'https://bookme-api.herokuapp.com',
    });
  }

  runFunction.$inject = ['$rootScope', '$location'];

  function runFunction($rootScope, $location) {
    $rootScope.$on('$routeChangeError', function(event, next, previous, error) {
      if (error === "AUTH_REQUIRED") {
        $location.path('/');
      }
    });
  }

})();
