(function() {
  'use strict';

  angular
    .module('app.auth')
    .factory('authService', authService);

  authService.$inject = ['$rootScope', '$auth', '$route'];

  function authService($rootScope, $auth, $route) {
    var currentUser;
    var registrationError;

    $rootScope.$on('auth:validation-success', authListener);
    $rootScope.$on('auth:login-success', authListener);

    $rootScope.$on('auth:logout-success', function(ev) {
      currentUser = null;
    });
    $rootScope.$on('auth:registration-email-error', function(ev, reason) {
      registrationError = reason.errors.full_messages;
      /*reason.errors.full_messages.forEach(function (item, index) {
        registrationError += item + ". \n";
      });*/
    });

    function authListener(ev, user) {
      currentUser = user;
    }

    var service = {
      register: register,
      login: login,
      logout: logout,
      isLoggedIn: isLoggedIn,
      isRegistrationError: isRegistrationError
    };

    return service;

    ////////////

    function register(user) {
      return $auth.submitRegistration(user);
    }

    function login(user) {
      return $auth.submitLogin(user);
    }

    function logout() {
      $rootScope.$broadcast('logout');
      $auth.signOut().then(function() {
        $route.reload();
        });
    }

    function isLoggedIn() {
      return currentUser;
    }
    function isRegistrationError() {
      return registrationError;
    }


  }

})();
