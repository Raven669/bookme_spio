(function() {
  'use strict';

  angular
    .module('app.list')
    .directive('gzBookssTable', gzBookssTable);

  function gzBookssTable() {
    return {
      templateUrl: 'app/list/directives/bookssTable.html',
      restrict: 'E',
      controller: BookssTableController,
      controllerAs: 'vm',
      bindToController: true,
      scope: {
        books: '=',
        loggedin: '='
      }
    }
  }

  BookssTableController.$inject = ['$scope'];

  function BookssTableController($scope) {

    $scope.sortType     = 'author.last_name'; // set the default sort type
    $scope.sortReverse  = false;  // set the default sort order
    

    $scope.sortType     = 'title'; // set the default sort type
    $scope.sortReverse  = false;  // set the default sort order

    $scope.sortType     = 'publisher.name'; // set the default sort type
    $scope.sortReverse  = false;  // set the default sort order

    $scope.sortType     = 'genre.name'; // set the default sort type
    $scope.sortReverse  = false;  // set the default sort order

    $scope.searchTitle   = '';     // set the default search/filter term

  }
})();
