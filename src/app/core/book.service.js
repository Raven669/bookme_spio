(function() {
  'use strict';

  angular
    .module('app.core')
    .factory('bookService', bookService);

  bookService.$inject = ['API_URL', '$http'];

  function bookService(API_URL, $http) {
    var service = {
      getBooks: getBooks,
      getBook: getBook,
    };

    return service;

    ////////////

    function getBooks() {
      return $http({
        method: 'GET',
        url: API_URL + 'books'
      }).then(function successCallback(response) {
        return response.data
      });
    }

    function getBook(id) {
      return $http({
        method: 'GET',
        url: API_URL + 'books/' + id
      }).then(function successCallback(response) {
        return response.data
      });
    }
  }

})();
