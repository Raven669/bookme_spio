module.exports = function(grunt) {

    // Project configuration.
    grunt.initConfig({
        connect: {
            server: {
                options: {
                    port: 8080,
                    hostname: '*',
                    livereload: true,
                    base: 'src'
                }
            }
        },
        selenium_standalone: {
            options: {
                stopOnExit: true
            },
            test: {
                seleniumVersion: '2.53.0',
                seleniumDownloadURL: 'http://selenium-release.storage.googleapis.com',
                drivers: {
                    chrome: {
                        version: '2.21',
                        arch: process.arch,
                        baseURL: 'http://chromedriver.storage.googleapis.com'
                    }
                }
            }
        },
        protractor: {
            options: {
                keepAlive: true,
                noColor: false,
                configFile: "test_e2e/protractor.conf.js",
                args: {}
            },

            local: {
                options: {
                    verbose: true,
                    keepAlive: false,
                    configFile: "test_e2e/protractor.conf.js",
                    args: {}
                }
            },
            browserStack: {
                options: {
                    configFile: 'test/browserStack.conf.js'
                }
            }
        }


    
    });


    // grunt.loadNpmTasks('grunt-contrib-clean');
    // grunt.loadNpmTasks('grunt-contrib-copy');
    // grunt.loadNpmTasks('grunt-browserify');
    // grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-connect');
    grunt.loadNpmTasks('grunt-selenium-standalone');
    grunt.loadNpmTasks('grunt-protractor-runner');

    // Default task(s).
    //grunt.registerTask('default', ['clean', 'copy', 'browserify', 'connect', 'watch']);

    grunt.registerTask('test', ['connect', 'selenium_standalone:test:install',
        'selenium_standalone:test:start', 'protractor:local']);

};
