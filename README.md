BookMe - praca/aplikacja dyplomowa stworzona na zaliczenie przedmiotu Projekt Informatyczny będącego częścią Studium Podyplomowego Inżynieria Oprogramowania realizowanego na Politechnice Poznańskiej rok akademicki 2015/2016. Aplikacja powstała w metodyce Agile w pięciu sprintach każdy po 8 godzin. Użyte technologie - Angular JS, Java Script, HTML 5, CSS 3, Bootstrap, Ruby , SQL, Protractor

twórcy :

Daniel Safiński - product owner
Tomasz Rychlicki - full stack developer
Marcin Jakubowski - full stack developer
Jacek Stachowiak - junior front-end developer
Michał Lubczyński - junior front-end developer
Katarzyna Abram - tester
Monika Kucharczyk - tester