(function() {
  'use strict';

  angular
    .module('app.auth')
    .controller('AuthController', AuthController);

  AuthController.$inject = ['$location', 'authService', '$rootScope'];

  function AuthController($location, authService, $rootScope) {
    var vm = this;

    $rootScope.bgimg = "";

    vm.register = register;
    vm.login = login;


    function register(user) {
      return authService.register(user)
        .then(function() {
          return vm.login(user);
        })
        .catch(function(response) {
          if(authService.isRegistrationError())
            vm.error = authService.isRegistrationError();
          else
          vm.error = response.errors;
        });
    }

    function login(user) {
      return authService.login(user)
        .then(function(response) {
          $location.path('/userbookslist');
          return response;
        })
        .catch(function(response) {
          vm.error = response.errors;
        });
    }
  }
})();

function checkPassword(pass) {
  return (pass.length >= 8);
};
