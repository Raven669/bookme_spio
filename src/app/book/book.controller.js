(function() {
    'use strict';

    angular
        .module('app.book')
        .controller('BookController', BookController);

    BookController.$inject = ['$rootScope', '$routeParams', 'bookService'];

    function BookController($rootScope, $routeParams, bookService) {
        var vm = this;

        bookService.getBook($routeParams.id).then(function(data) {
          vm.book = data;
        });
    }

})();
