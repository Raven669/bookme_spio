(function () {
    'use strict';

    angular
        .module('app.userBooksList')
        .directive('gzBooksTable', gzBooksTable);

    function gzBooksTable() {
        return {
            templateUrl: 'app/userBooksList/directives/booksTable.html',
            restrict: 'E',
            controller: BooksTableController,
            controllerAs: 'vm',
            bindToController: true,
            scope: {
                offers: '='
            }
        }
    }

    BooksTableController.$inject = ['offerService'];

    function BooksTableController(offerService) {
        var vm = this;

        vm.toggleOffer = toggleOffer;
        vm.deleteOffer = deleteOffer;

        function toggleOffer(offer) {
            offerService.updateOffer(offer.id).then(function (response) {
                updateOffers();
            });
        }

        function deleteOffer(offer) {
            offerService.deleteOffer(offer.id).then(function (response) {
                updateOffers();
            });
        }

        function updateOffers() {
            offerService.getOffersByCurrentUser().then(function (response) {
                vm.offers = response;
            });
        }
    }


    BookssTableController.$inject = ['$scope'];

    function BookssTableController($scope) {

        $scope.sortType = 'author.last_name'; // set the default sort type
        $scope.sortReverse = false;  // set the default sort order


        $scope.sortType = 'title'; // set the default sort type
        $scope.sortReverse = false;  // set the default sort order

        $scope.sortType = 'publisher.name'; // set the default sort type
        $scope.sortReverse = false;  // set the default sort order

        $scope.sortType = 'genre.name'; // set the default sort type
        $scope.sortReverse = false;  // set the default sort order

        $scope.sortType = 'isbn'; // set the default sort type
        $scope.sortReverse = false;  // set the default sort order


        $scope.searchTitle = '';     // set the default search/filter term

    }


})();
