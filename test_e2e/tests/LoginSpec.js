var LoginPage = function() {
    this.containerNgScope = element(by.css(".container"));
    this.loginButton = this.containerNgScope.all(by.css('.btn.btn-lg.btn-primary.btn-block'));
    this.email = this.containerNgScope.all(by.model('vm.user.email'));
    this.password = this.containerNgScope.all(by.model('vm.user.password'));
};
//Test to check that "Login" button is present on the main page
describe('http://127.0.0.1:8080/#/login', function() {
    var loginPage = new LoginPage();
    beforeEach(function () {
        browser.get('http://127.0.0.1:8080/#/login');
    });
    it('should have one button', function () {
        expect(loginPage.loginButton.isDisplayed());
    });
//Test to check that user can input valid credentials
    it('should log in', function () {
        // Find page elements
        var email = element(by.model('vm.user.email'));
        var password = element(by.model('vm.user.password'));
        var loginButton = element(by.css('input[value="Zaloguj"]'));
        // Fill input fields
        email.sendKeys('first@example.com');
        password.sendKeys('password');
        // Ensure fields contain what we've entered
        expect(email.getAttribute('value')).toEqual('first@example.com');
        expect(password.getAttribute('value')).toEqual('password');
        // Click to log in and be redirected to user books list
        loginButton.click();
        //browser.pause()
    });
    //Test to check that user is redirected to user books list
    it('should log in', function () {

        // Find page elements
        var email = element(by.model('vm.user.email'));
        var password = element(by.model('vm.user.password'));
        var loginButton = element(by.css('input[type="submit"]'));

        // Fill input fields
        email.sendKeys('second@example.com');
        password.sendKeys('password');

        // Ensure fields contain what we've entered
        expect(email.getAttribute('value')).toEqual('second@example.com');
        expect(password.getAttribute('value')).toEqual('password');

        // Click to log in and be redirected to user books list
        loginButton.click();
        //browser.pause()
        expect(browser.getCurrentUrl()).toEqual('http://127.0.0.1:8080/#/userbookslist');

    });
});
