(function() {
  'use strict';

  angular
    .module('app.core')
    .constant('FIREBASE_URL', 'https://glowing-torch-6596.firebaseio.com/')
    .constant('API_URL', 'https://bookme-api.herokuapp.com/api/v1/');
})();
