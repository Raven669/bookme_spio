/**
 * Created by tomaszr on 2016-04-24.
 */
(function() {
    'use strict';

    angular
        .module('app.list')
        .controller('ListController', ListController);

    ListController.$inject = ['$rootScope', 'bookService', 'authService'];

    function ListController($rootScope, bookService, authService) {
        var vm = this;

        bookService.getBooks().then(function(data) {
          vm.books = data;
        });
        vm.loggedin = (authService.isLoggedIn() != null);
            
    }

})();
