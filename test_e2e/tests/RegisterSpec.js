   describe('http://127.0.0.1:8080/#/', function() {
        var homePage = new HomePage();
        beforeEach(function() {
            browser.get('http://127.0.0.1:8080/#/');
        });

        var registerPage = new RegisterPage();
        function HomePage() {
           this.starterTemplate = element(by.css(".starter-template"));
           this.ZarejestrujSięButton = this.starterTemplate.all(by.css(".btn-primary"));
        }
        function RegisterPage() {
            this.emailField = element(by.model("vm.user.email"));
            this.passwordField = element(by.model("vm.user.password"));
            this.signUpButton = element(by.css('.btn.btn-lg.btn-primary'));
        }

        RegisterPage.prototype.visitPage = function () {
            browser.get('http://127.0.0.1:8080/#/register');
        };

        RegisterPage.prototype.fillEmail = function (email) {
            this.emailField.sendKeys(email);
        };

        RegisterPage.prototype.fillPassword = function (password) {
            if (password == null) {
                password = "password";
            }
            this.passwordField.sendKeys(password);
        };

        RegisterPage.prototype.register = function () {
            this.signUpButton.click();
        };

        it('should have one button', function () {
            // given
            // when
            // then
            expect(homePage.ZarejestrujSięButton.isDisplayed());
        });

        it('should open Register Page', function () {
            // given

            // when
            registerPage.visitPage();

            // then
            expect(browser.getCurrentUrl()).toMatch('http://127.0.0.1:8080/#/register');
        });


        it('user should register on page', function() {
            // given
            registerPage.visitPage();
            registerPage.fillEmail("a@a.pl");
            registerPage.fillPassword("1234");

            // when
            registerPage.register();

            // then
            expect(browser.getCurrentUrl()).toMatch('http://127.0.0.1:8080/#/register');
        });

        //return HomePage;
        });


