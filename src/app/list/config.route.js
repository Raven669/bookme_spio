(function() {
  'use strict';

  angular
    .module('app.list')
    .config(configFunction);

  configFunction.$inject = ['$routeProvider'];

  function configFunction($routeProvider) {
    $routeProvider.when('/list', {
      templateUrl: 'app/list/list.html',
      controller: 'ListController',
      controllerAs: 'vm'
    });
  }
})();
